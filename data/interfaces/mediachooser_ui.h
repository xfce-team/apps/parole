/* automatically generated from mediachooser.ui */
#ifdef __SUNPRO_C
#pragma align 4 (mediachooser_ui)
#endif
#ifdef __GNUC__
static const char mediachooser_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char mediachooser_ui[] =
#endif
{
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><interface><requires lib=\"gt"
  "k+\" version=\"3.20\"/><object class=\"GtkSpinner\" id=\"spinner\"><pro"
  "perty name=\"can_focus\">False</property><property name=\"no_show_all\""
  ">True</property><property name=\"active\">True</property></object><obje"
  "ct class=\"GtkDialog\" id=\"chooser\"><property name=\"can_focus\">Fals"
  "e</property><property name=\"border_width\">0</property><property name="
  "\"modal\">True</property><property name=\"default_width\">640</property"
  "><property name=\"default_height\">480</property><property name=\"icon_"
  "name\">org.xfce.parole</property><property name=\"type_hint\">dialog</p"
  "roperty><signal name=\"destroy\" handler=\"parole_media_chooser_destroy"
  "_cb\" swapped=\"no\"/><child type=\"titlebar\"><object class=\"GtkHeade"
  "rBar\" id=\"titlebar\"><property name=\"visible\">True</property><prope"
  "rty name=\"can_focus\">False</property><property name=\"title\" transla"
  "table=\"yes\">Open Media Files</property><property name=\"has_subtitle\""
  ">False</property><child><object class=\"GtkButton\" id=\"close\"><prope"
  "rty name=\"label\">Cancel</property><property name=\"visible\">True</pr"
  "operty><property name=\"can_focus\">True</property><property name=\"rec"
  "eives_default\">True</property><signal name=\"clicked\" handler=\"parol"
  "e_media_chooser_close_clicked\" swapped=\"no\"/></object></child><child"
  "><object class=\"GtkButton\" id=\"open\"><property name=\"label\">Open<"
  "/property><property name=\"visible\">True</property><property name=\"ca"
  "n_focus\">True</property><property name=\"can_default\">True</property>"
  "<property name=\"has_default\">True</property><property name=\"receives"
  "_default\">True</property><signal name=\"clicked\" handler=\"parole_med"
  "ia_chooser_add_clicked\" swapped=\"no\"/><style><class name=\"suggested"
  "-action\"/></style></object><packing><property name=\"pack_type\">end</"
  "property><property name=\"position\">1</property></packing></child></ob"
  "ject></child><child internal-child=\"vbox\"><object class=\"GtkBox\" id"
  "=\"dialog-vbox1\"><property name=\"can_focus\">False</property><propert"
  "y name=\"orientation\">vertical</property><property name=\"spacing\">2<"
  "/property><child internal-child=\"action_area\"><object class=\"GtkButt"
  "onBox\" id=\"dialog-action_area1\"><property name=\"can_focus\">False</"
  "property><property name=\"visible\">True</property><property name=\"lay"
  "out_style\">end</property><child><placeholder/></child></object><packin"
  "g><property name=\"expand\">False</property><property name=\"fill\">Fal"
  "se</property><property name=\"pack_type\">end</property><property name="
  "\"position\">0</property></packing></child><child><object class=\"GtkFi"
  "leChooserWidget\" id=\"filechooserwidget\"><property name=\"visible\">T"
  "rue</property><property name=\"can_focus\">False</property><property na"
  "me=\"border_width\">0</property><property name=\"extra_widget\">spinner"
  "</property><property name=\"select_multiple\">True</property><signal na"
  "me=\"current-folder-changed\" handler=\"media_chooser_folder_changed_cb"
  "\" swapped=\"no\"/><signal name=\"file-activated\" handler=\"media_choo"
  "ser_file_activate_cb\" swapped=\"no\"/></object><packing><property name"
  "=\"expand\">True</property><property name=\"fill\">True</property><prop"
  "erty name=\"position\">1</property></packing></child></object></child><"
  "/object></interface>"
};

static const unsigned mediachooser_ui_length = 3119u;

